---
layout: default
title: "Inscriu-te"
---

## Inscriu-te al Congrés

No és obligatoria la inscripció però sí que ens ajuda a preveure quanta gent vindrà.
En cas que se superés l'aforament d'una sala, es donaria prioritat a les persones inscrites.

Només cal omplir aquest [formulari](https://formularis.sobtec.cat/inscripcions-sobtec-2020)

## Vols venir a dinar?

La [Capirota](https://bcn.coop/portfolio/capirota/) ofereix un menú per a 7 euros (es paga el mateix dia) amb variants vegetarianes, carnívores i per a intolerants al gluten.

Si et portes el teu menjar, pots apuntar-te i et buscarem un lloc.

El formulari el tens [aquí](https://formularis.sobtec.cat/dinar-sobtec-2020)