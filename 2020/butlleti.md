---
layout: default
title: "Butlletí"
---

## Butlletí de Congrés

Si vols rebre informació actualitzada del proper Congrés només cal que t'inscriguis al nostre butlletí.

Ens comprometem a només enviar informació relacionada amb el congrés.

També ens pots seguir per nostre compte de mastodons i twitter.
