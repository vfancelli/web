---
layout: default
title: "Edicions anteriors"
---

[Congrés 2019](/2019)

[Congrés 2018](/2018)

[Congrés 2017](/2017)

[Congrés 2016](/2016)
